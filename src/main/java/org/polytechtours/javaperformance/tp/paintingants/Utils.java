package org.polytechtours.javaperformance.tp.paintingants;

public class Utils {

	public static int getRed(int color) {
		return (color >> 16) & 0xFF;
	}

	/**
	 * Returns the green component in the range 0-255 in the default sRGB
	 * space.
	 * @return the green component.
	 */
	public static int getGreen(int color) {
		return (color >> 8) & 0xFF;
	}

	/**
	 * Returns the blue component in the range 0-255 in the default sRGB
	 * space.
	 * @return the blue component.
	 */
	public static int getBlue(int color) {
		return (color >> 0) & 0xFF;
	}

	/**
	 * Returns the alpha component in the range 0-255.
	 * @return the alpha component.
	 */
	public static int getAlpha(int color) {
		return (color >> 24) & 0xff;
	}

	public static int toRGB(int r, int g, int b){
		return ((255 & 0xFF) << 24) |
				((r & 0xFF) << 16) |
				((g & 0xFF) << 8)  |
				((b & 0xFF) << 0);
	}

	public static int toRGB(int r, int g, int b, int a){
		return ((a & 0xFF) << 24) |
				((r & 0xFF) << 16) |
				((g & 0xFF) << 8)  |
				((b & 0xFF) << 0);
	}
}
