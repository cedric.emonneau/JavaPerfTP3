package org.polytechtours.javaperformance.tp.paintingants;
// package PaintingAnts_v3;
// version : 4.0

import java.awt.Color;
import java.util.Random;

public class Fourmi {
	// Tableau des incrémentations à effectuer sur la position des fourmis
	// en fonction de la direction du deplacement
	static private int[][] mIncDirection = new int[8][2];
	// le generateur aléatoire (Random est thread safe donc on la partage)
	private static Random generateurAleatoire = new Random();
	// couleur déposé par la fourmi
	private Color couleurDeposee;
	private float mLuminanceCouleurSuivie;
	// objet graphique sur lequel les fourmis peuvent peindre
	private OutputCanvas outputCanvas;
	// Coordonées de la fourmi
	private int x, y;
	// Proba d'aller a gauche, en face, a droite, de suivre la couleur
	private float[] mProba = new float[4];
	// Numéro de la direction dans laquelle la fourmi regarde
	private int mDirection;
	// Taille de la trace de phéromones déposée par la fourmi
	private int mTaille;
	// Pas d'incrémentation des directions suivant le nombre de directions
	// allouées à la fourmies
	private int mDecalDir;
	// l'applet
	private Application mApplis;
	// seuil de luminance pour la détection de la couleur recherchée
	private float mSeuilLuminance;
	// nombre de déplacements de la fourmi
	private long mNbDeplacements;

	/*************************************************************************************************
	 */
	public Fourmi(Color pCouleurDeposee, Color pCouleurSuivie, float pProbaTD, float pProbaG, float pProbaD,
				  float pProbaSuivre, OutputCanvas pOutputCanvas, char pTypeDeplacement, float pInit_x, float pInit_y, int pInitDirection,
				  int pTaille, float pSeuilLuminance, Application pApplis) {

		couleurDeposee = pCouleurDeposee;
		mLuminanceCouleurSuivie = 0.2426f * pCouleurDeposee.getRed() + 0.7152f * pCouleurDeposee.getGreen()
				+ 0.0722f * pCouleurDeposee.getBlue();
		outputCanvas = pOutputCanvas;
		mApplis = pApplis;

		// direction de départ
		mDirection = pInitDirection;

		// taille du trait
		mTaille = pTaille;

		// initialisation des probas
		mProba[0] = pProbaG; // proba d'aller à gauche
		mProba[1] = pProbaTD; // proba d'aller tout droit
		mProba[2] = pProbaD; // proba d'aller à droite
		mProba[3] = pProbaSuivre; // proba de suivre la couleur

		// nombre de directions pouvant être prises : 2 types de déplacement
		// possibles
		if (pTypeDeplacement == 'd') {
			mDecalDir = 2;
		} else {
			mDecalDir = 1;
		}

		// initialisation du tableau des directions
		Fourmi.mIncDirection[0][0] = 0;
		Fourmi.mIncDirection[0][1] = -1;
		Fourmi.mIncDirection[1][0] = 1;
		Fourmi.mIncDirection[1][1] = -1;
		Fourmi.mIncDirection[2][0] = 1;
		Fourmi.mIncDirection[2][1] = 0;
		Fourmi.mIncDirection[3][0] = 1;
		Fourmi.mIncDirection[3][1] = 1;
		Fourmi.mIncDirection[4][0] = 0;
		Fourmi.mIncDirection[4][1] = 1;
		Fourmi.mIncDirection[5][0] = -1;
		Fourmi.mIncDirection[5][1] = 1;
		Fourmi.mIncDirection[6][0] = -1;
		Fourmi.mIncDirection[6][1] = 0;
		Fourmi.mIncDirection[7][0] = -1;
		Fourmi.mIncDirection[7][1] = -1;

		mSeuilLuminance = pSeuilLuminance;
		mNbDeplacements = 0;
	}

	/*************************************************************************************************
	 * Titre : void deplacer() Description : Fonction de deplacement de la fourmi
	 *
	 */
	public void deplacer() {
		float tirage, prob1, prob2, prob3, total;
		int[] dir = {0, 0, 0};
		int i, j;

		mNbDeplacements++;

		// le tableau dir contient 0 si la direction concernée ne contient pas la
		// couleur
		// à suivre, et 1 sinon (dir[0]=gauche, dir[1]=tt_droit, dir[2]=droite)
		i = modulo(x + Fourmi.mIncDirection[modulo(mDirection - mDecalDir, 8)][0], outputCanvas.getLargeur());
		j = modulo(y + Fourmi.mIncDirection[modulo(mDirection - mDecalDir, 8)][1], outputCanvas.getHauteur());
		setDirection(i, j, 0, dir);

		i = modulo(x + Fourmi.mIncDirection[mDirection][0], outputCanvas.getLargeur());
		j = modulo(y + Fourmi.mIncDirection[mDirection][1], outputCanvas.getHauteur());
		setDirection(i, j, 1, dir);

		i = modulo(x + Fourmi.mIncDirection[modulo(mDirection + mDecalDir, 8)][0], outputCanvas.getLargeur());
		j = modulo(y + Fourmi.mIncDirection[modulo(mDirection + mDecalDir, 8)][1], outputCanvas.getHauteur());
		setDirection(i, j, 2, dir);

		// tirage d'un nombre aléatoire permettant de savoir si la fourmi va suivre
		// ou non la couleur
		tirage = generateurAleatoire.nextFloat();// Math.random();

		// la fourmi suit la couleur
		if (((tirage <= mProba[3]) && ((dir[0] + dir[1] + dir[2]) > 0)) || ((dir[0] + dir[1] + dir[2]) == 3)) {
			prob1 = (dir[0]) * mProba[0];
			prob2 = (dir[1]) * mProba[1];
			prob3 = (dir[2]) * mProba[2];
		}
		// la fourmi ne suit pas la couleur
		else {
			prob1 = (1 - dir[0]) * mProba[0];
			prob2 = (1 - dir[1]) * mProba[1];
			prob3 = (1 - dir[2]) * mProba[2];
		}
		total = prob1 + prob2 + prob3;
		prob1 = prob1 / total;
		prob2 = prob2 / total + prob1;
		prob3 = prob3 / total + prob2;

		// incrémentation de la direction de la fourmi selon la direction choisie
		tirage = generateurAleatoire.nextFloat();// Math.random();
		if (tirage < prob1) {
			mDirection = modulo(mDirection - mDecalDir, 8);
		} else {
			if (tirage < prob2) {
		/* rien, on va tout droit */
			} else {
				mDirection = modulo(mDirection + mDecalDir, 8);
			}
		}

		x += Fourmi.mIncDirection[mDirection][0];
		y += Fourmi.mIncDirection[mDirection][1];

		x = modulo(x, outputCanvas.getLargeur());
		y = modulo(y, outputCanvas.getHauteur());

		// coloration de la nouvelle position de la fourmi
		outputCanvas.setCouleur(x, y, couleurDeposee.getRed(), couleurDeposee.getGreen(), couleurDeposee.getBlue(), mTaille);
	}

	private void setDirection(int i, int j, int indexDir, int[] dir){
		int baseImageColor = outputCanvas.getCouleur(i, j);
		int red = (baseImageColor & 0xff0000) >> 16;
		int green = (baseImageColor & 0xff00) >> 8;
		int blue = baseImageColor & 0xff;

		dir[indexDir] = testCouleur(red,green,blue) ? 1 : 0;
	}

	/*************************************************************************************************
	 */
	public long getNbDeplacements() {
		return mNbDeplacements;
	}
	/****************************************************************************/

	/*************************************************************************************************
	 */
	public int getX() {
		return x;
	}

	/*************************************************************************************************
	 */
	public int getY() {
		return y;
	}

	/*************************************************************************************************
	 * Titre : modulo Description : Fcontion de modulo permettant au fourmi de
	 * reapparaitre de l autre coté du Canvas lorsque qu'elle sorte de ce dernier
	 *
	 * @param x
	 *          valeur
	 *
	 * @return int
	 */
	private int modulo(int x, int m) {
		return (x + m) % m;
	}

	/*************************************************************************************************
	 * Titre : boolean testCouleur() Description : fonction testant l'égalité
	 * d'une couleur avec la couleur suivie
	 *
	 */
	private boolean testCouleur(int red, int green, int blue) {
		float lLuminance;
    	/* on calcule la luminance */
		lLuminance = 0.2426f * red + 0.7152f * green + 0.0722f * blue;
		return Math.abs(mLuminanceCouleurSuivie - lLuminance) < mSeuilLuminance;
	}
}
