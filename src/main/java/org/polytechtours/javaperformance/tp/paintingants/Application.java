package org.polytechtours.javaperformance.tp.paintingants;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.Timer;

public class Application extends java.applet.Applet implements Runnable,KeyListener {
	private static final long serialVersionUID = 1L;
	// parametres
	private int mLargeur;
	private int mHauteur;

	private Framerate framerate = new Framerate();

	// l'objet graphique lui meme
	private OutputCanvas mOutputCanvas;

	// les fourmis
	private Vector<Fourmi> mColonie = new Vector<Fourmi>();
	private Colonie mColony;

	private Thread mApplis, mThreadColony;

	private Dimension mDimension;
	private boolean mPause = false;

	public BufferedImage mBaseImage;
	private Timer fpsTimer;

//	private int drawEachXFrames = 60;

	/**
	 * Fourmis per second :)
	 */
//	private Long fpsCounter = 0L;
	/**
	 * stocke la valeur du compteur lors du dernier timer
	 */
//	private Long lastFps = 0L;

	public Application() throws HeadlessException {
		super();
	}

	/****************************************************************************/
	/**
	 * Détruire l'applet
	 */
	@Override
	public void destroy() {
		if (mApplis != null) {
			mApplis = null;
		}
	}

	/****************************************************************************/
	/**
	 * Obtenir l'information Applet
	 */
	@Override
	public String getAppletInfo() {
		return "OutputCanvas Ants";
	}

	/****************************************************************************/
	/**
	 * Obtenir l'information Applet
	 */

	@Override
	public String[][] getParameterInfo() {
		String[][] lInfo = {{"SeuilLuminance", "string", "Seuil de luminance"}, {"Img", "string", "Image"},
				{"NbFourmis", "string", "Nombre de fourmis"}, {"Fourmis", "string",
				"Paramètres des fourmis (RGB_déposée)(RGB_suivie)(x,y,direction,taille)(TypeDeplacement,ProbaG,ProbaTD,ProbaD,ProbaSuivre);...;"}};
		return lInfo;
	}

	/****************************************************************************/
	/**
	 * Obtenir l'état de pause
	 */
	public boolean getPause() {
		return mPause;
	}

	/****************************************************************************/
	/**
	 * Initialisation de l'applet
	 */
	@Override
	public void init() {
		URL lFileName;
		URLClassLoader urlLoader = (URLClassLoader) this.getClass().getClassLoader();

		// lecture des parametres de l'applet

		mDimension = getSize();
		mLargeur = mDimension.width;
		mHauteur = mDimension.height;

		mOutputCanvas = new OutputCanvas(mDimension, this);
		add(mOutputCanvas);

		// lecture de l'image
		lFileName = urlLoader.findResource("images/" + getParameter("Img"));
		try {
			if (lFileName != null) {
				mBaseImage = javax.imageio.ImageIO.read(lFileName);
			}
		} catch (java.io.IOException ex) {
		}

		if (mBaseImage != null) {
			mLargeur = mBaseImage.getWidth();
			mHauteur = mBaseImage.getHeight();
			mDimension.setSize(mLargeur, mHauteur);
			resize(mDimension);
		}

		setLayout(null);

		System.out.println("Add listener");
		addKeyListener(this);
		setFocusable(true);
		requestFocusInWindow();
	}

	/****************************************************************************/
	/**
	 * Paint the image and all active highlights.
	 */
	@Override
	public void paint(Graphics g) {
		if (mBaseImage == null) {
			return;
		}
		g.drawImage(mBaseImage, 0, 0, this);
	}
	/****************************************************************************/

	/****************************************************************************/
	/**
	 * Mettre en pause
	 */
	public void pause() {
		mPause = !mPause;
		// if (!mPause)
		// {
		// notify();
		// }
	}


	/*************************************************************************************************
	 * Titre : boolean testCouleur() Description : fonction testant l'égalité de
	 * deux couleurs
	 *
	 */
	@Override
	public void run() {
		// System.out.println(this.getName()+ ":run()");

		int i;
		StringBuilder lMessage;

		mOutputCanvas.init();

		Thread currentThread = Thread.currentThread();

		mThreadColony.start();

		while (mApplis == currentThread) {
			if (mPause) {
				lMessage = new StringBuilder("pause");
				showStatus(lMessage.toString());
			} else {
				lMessage = new StringBuilder("running (" + framerate.getLast() + ") ");
				long lFramerate = framerate.getLast();
				for (i = 0; i < lFramerate%10000 / 1000; i++) {
					lMessage.append(".");
				}

				showStatus(lMessage.toString());
			}

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				showStatus(e.toString());
			}
		}
	}

	/****************************************************************************/
	/**
	 * Lancer l'applet
	 */
	@Override
	public void start() {
		mColony = new Colonie( this, framerate, mOutputCanvas);
		mThreadColony = new Thread(mColony);
		mThreadColony.setPriority(Thread.MIN_PRIORITY);

		fpsTimer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				framerate.reset();
			}
		});
		fpsTimer.setRepeats(true);
		fpsTimer.start();

		showStatus("starting...");
		// Create the thread.
		mApplis = new Thread(this);
		// and let it start running
		mApplis.setPriority(Thread.MIN_PRIORITY);
		mApplis.start();
	}

	/****************************************************************************/
	/**
	 * Arrêter l'applet
	 */
	@Override
	public void stop() {
		showStatus("stopped...");

		fpsTimer.stop();

		// On demande au Thread Colony de s'arreter et on attend qu'il s'arrete
		mColony.pleaseStop();
		try {
			mThreadColony.wait();
		} catch (Exception e) {
		}

		mThreadColony = null;
		mApplis = null;
	}


	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyChar() == 'i') {
			init();
		} else if (e.getKeyChar() == 'p') {
			pause();
		} else if (e.getKeyChar() == 't') {
			mOutputCanvas.toggleSimulation();
		}
		e.consume();
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}
}
