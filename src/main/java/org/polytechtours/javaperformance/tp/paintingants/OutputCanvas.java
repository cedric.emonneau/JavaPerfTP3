package org.polytechtours.javaperformance.tp.paintingants;
// package PaintingAnts_v2;

import javax.swing.*;
import java.awt.*;

// version : 2.0

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.WritableRaster;

/**
 * <p>
 * Titre : OutputCanvas Ants
 * </p>
 * <p>
 * Description :
 * </p>
 * <p>
 * Copyright : Copyright (c) 2003
 * </p>
 * <p>
 * Société : Equipe Réseaux/TIC - Laboratoire d'Informatique de l'Université de
 * Tours
 * </p>
 *
 * @author Nicolas Monmarché
 * @version 1.0
 */

public class OutputCanvas extends JPanel{
	private static final long serialVersionUID = 1L;
	// matrice servant pour le produit de convolution
	static private float[][] mMatriceConv9 = new float[3][3];
	static private float[][] mMatriceConv25 = new float[5][5];
	static private float[][] mMatriceConv49 = new float[7][7];
	// Objet de type Graphics permettant de manipuler l'affichage du Canvas
	private Graphics mGraphics;
	// Objet ne servant que pour les bloc synchronized pour la manipulation du
	// tableau des couleurs, il permert de conserver en memoire l'état de chaque
	// pixel du canvas, ce qui est necessaire au deplacemet des fourmi
	// il sert aussi pour la fonction paint du Canvas
	private int[][] mCouleurs;
	// couleur du fond
	private Color mCouleurFond = new Color(255, 255, 255);
	// dimensions
	private Dimension mDimension = new Dimension();

	private Application mApplis;

	private boolean mSuspendu = false;

	private int bufferedCoordinatesMSmooth[][][][];
	private int bufferedCoordinatesNSmooth[][][][];
	private int bufferedCoordinatesMSmoothDest[][][];
	private int bufferedCoordinatesNSmoothDest[][][];
	private boolean updateOnGraphic[][];


	BufferedImage backBufferImage;
	// I used Graphics2D instead of Graphics here, because its more flexible and lets you do more things.
//	Graphics2D backBufferGraphics;

	/******************************************************************************
	 * Titre : public OutputCanvas() Description : Constructeur de la classe
	 ******************************************************************************/
	public OutputCanvas(Dimension pDimension, Application pApplis) {
		mApplis = pApplis;

		mDimension = pDimension;
		setBounds(new Rectangle(0, 0, mDimension.width, mDimension.height));

		this.setBackground(mCouleurFond);

		// initialisation de la matrice des couleurs
		mCouleurs = new int[mDimension.width][mDimension.height];
		for (int i = 0; i != mDimension.width; i++) {
			for (int j = 0; j != mDimension.height; j++) {
				mCouleurs[i][j] = Utils.toRGB(mCouleurFond.getRed(), mCouleurFond.getGreen(), mCouleurFond.getBlue());
			}
		}




		bufferedCoordinatesMSmooth = new int[3][][][];
		for(int pTaille = 1; pTaille <= 3; ++pTaille) {
			bufferedCoordinatesMSmooth[pTaille-1] = new int[mDimension.width][][];
			for(int x = 0; x < mDimension.width; ++x){
				int virtualSize = 1 + pTaille * 2;
				int pTaille2 = pTaille * 2;
				bufferedCoordinatesMSmooth[pTaille-1][x] = new int[virtualSize][];

				for (int i = 0; i < virtualSize; i++) {
					bufferedCoordinatesMSmooth[pTaille-1][x][i] = new int[virtualSize];
					for (int k = 0; k < virtualSize; k++) {
						bufferedCoordinatesMSmooth[pTaille-1][x][i][k] = (x + i + k - pTaille2 + mDimension.width) % mDimension.width;
					}
				}
			}
		}


		bufferedCoordinatesNSmooth = new int[3][][][];
		for(int pTaille = 1; pTaille <= 3; ++pTaille) {
			bufferedCoordinatesNSmooth[pTaille-1] = new int[mDimension.height][][];
			for(int y = 0; y < mDimension.height; ++y){
				int virtualSize = 1 + pTaille * 2;
				int pTaille2 = pTaille * 2;
				bufferedCoordinatesNSmooth[pTaille-1][y] = new int[virtualSize][];

				for (int j = 0; j < virtualSize; j++) {
					bufferedCoordinatesNSmooth[pTaille-1][y][j] = new int[virtualSize];
					for (int l = 0; l < virtualSize; l++) {
						bufferedCoordinatesNSmooth[pTaille-1][y][j][l] = (y + j + l - pTaille2 + mDimension.height) % mDimension.height;
					}
				}
			}
		}



		bufferedCoordinatesMSmoothDest = new int[3][][];
		for(int pTaille = 1; pTaille <= 3; ++pTaille) {
			bufferedCoordinatesMSmoothDest[pTaille-1] = new int[mDimension.width][];
			for(int x = 0; x < mDimension.width; ++x){
				int virtualSize = 1 + pTaille * 2;
				bufferedCoordinatesMSmoothDest[pTaille-1][x] = new int[virtualSize];
				for (int i = 0; i < virtualSize; i++) {
					bufferedCoordinatesMSmoothDest[pTaille-1][x][i] = (x + i - pTaille + mDimension.width) % mDimension.width;
				}
			}
		}

		bufferedCoordinatesNSmoothDest = new int[3][][];
		for(int pTaille = 1; pTaille <= 3; ++pTaille) {
			bufferedCoordinatesNSmoothDest[pTaille-1] = new int[mDimension.height][];
			for(int y = 0; y < mDimension.height; ++y){
				int virtualSize = 1 + pTaille * 2;
				bufferedCoordinatesNSmoothDest[pTaille-1][y] = new int[virtualSize];
				for (int j = 0; j < virtualSize; j++) {
					bufferedCoordinatesNSmoothDest[pTaille-1][y][j] = (y + j - pTaille + mDimension.height) % mDimension.height;
				}
			}
		}

		updateOnGraphic = new boolean[mDimension.width][];
		for(int x = 0; x < mDimension.width; ++x){
			updateOnGraphic[x] = new boolean[mDimension.height];
			for(int y = 0; y < mDimension.height; ++y) {
				updateOnGraphic[x][y]=true;
			}
		}


	}

	DataBuffer outPixels;

	/******************************************************************************
	 * Titre : Color getCouleur Description : Cette fonction renvoie la couleur
	 * d'une case
	 ******************************************************************************/
	public int getCouleur(int x, int y) {
		return mCouleurs[x][y];
	}

	/******************************************************************************
	 * Titre : Color getDimension Description : Cette fonction renvoie la
	 * dimension de la peinture
	 ******************************************************************************/
	public Dimension getDimension() {
		return mDimension;
	}

	/******************************************************************************
	 * Titre : Color getHauteur Description : Cette fonction renvoie la hauteur de
	 * la peinture
	 ******************************************************************************/
	public int getHauteur() {
		return mDimension.height;
	}

	/******************************************************************************
	 * Titre : Color getLargeur Description : Cette fonction renvoie la hauteur de
	 * la peinture
	 ******************************************************************************/
	public int getLargeur() {
		return mDimension.width;
	}

	/******************************************************************************
	 * Titre : void init() Description : Initialise le fond a la couleur blanche
	 * et initialise le tableau des couleurs avec la couleur blanche
	 ******************************************************************************/
	public void init() {
		int i, j;
		mGraphics = getGraphics();
		mGraphics.clearRect(0, 0, mDimension.width, mDimension.height);

		// initialisation de la matrice des couleurs
		for (i = 0; i != mDimension.width; i++) {
			for (j = 0; j != mDimension.height; j++) {
				mCouleurs[i][j] = mCouleurFond.getRGB();
			}
		}

		// initialisation de la matrice de convolution : lissage moyen sur 9
		// cases
	/*
     * 1 2 1 2 4 2 1 2 1
     */
		OutputCanvas.mMatriceConv9[0][0] = 1 / 16f;
		OutputCanvas.mMatriceConv9[0][1] = 2 / 16f;
		OutputCanvas.mMatriceConv9[0][2] = 1 / 16f;
		OutputCanvas.mMatriceConv9[1][0] = 2 / 16f;
		OutputCanvas.mMatriceConv9[1][1] = 4 / 16f;
		OutputCanvas.mMatriceConv9[1][2] = 2 / 16f;
		OutputCanvas.mMatriceConv9[2][0] = 1 / 16f;
		OutputCanvas.mMatriceConv9[2][1] = 2 / 16f;
		OutputCanvas.mMatriceConv9[2][2] = 1 / 16f;

		// initialisation de la matrice de convolution : lissage moyen sur 25
		// cases
    /*
     * 1 1 2 1 1 1 2 3 2 1 2 3 4 3 2 1 2 3 2 1 1 1 2 1 1
     */
		OutputCanvas.mMatriceConv25[0][0] = 1 / 44f;
		OutputCanvas.mMatriceConv25[0][1] = 1 / 44f;
		OutputCanvas.mMatriceConv25[0][2] = 2 / 44f;
		OutputCanvas.mMatriceConv25[0][3] = 1 / 44f;
		OutputCanvas.mMatriceConv25[0][4] = 1 / 44f;
		OutputCanvas.mMatriceConv25[1][0] = 1 / 44f;
		OutputCanvas.mMatriceConv25[1][1] = 2 / 44f;
		OutputCanvas.mMatriceConv25[1][2] = 3 / 44f;
		OutputCanvas.mMatriceConv25[1][3] = 2 / 44f;
		OutputCanvas.mMatriceConv25[1][4] = 1 / 44f;
		OutputCanvas.mMatriceConv25[2][0] = 2 / 44f;
		OutputCanvas.mMatriceConv25[2][1] = 3 / 44f;
		OutputCanvas.mMatriceConv25[2][2] = 4 / 44f;
		OutputCanvas.mMatriceConv25[2][3] = 3 / 44f;
		OutputCanvas.mMatriceConv25[2][4] = 2 / 44f;
		OutputCanvas.mMatriceConv25[3][0] = 1 / 44f;
		OutputCanvas.mMatriceConv25[3][1] = 2 / 44f;
		OutputCanvas.mMatriceConv25[3][2] = 3 / 44f;
		OutputCanvas.mMatriceConv25[3][3] = 2 / 44f;
		OutputCanvas.mMatriceConv25[3][4] = 1 / 44f;
		OutputCanvas.mMatriceConv25[4][0] = 1 / 44f;
		OutputCanvas.mMatriceConv25[4][1] = 1 / 44f;
		OutputCanvas.mMatriceConv25[4][2] = 2 / 44f;
		OutputCanvas.mMatriceConv25[4][3] = 1 / 44f;
		OutputCanvas.mMatriceConv25[4][4] = 1 / 44f;

		// initialisation de la matrice de convolution : lissage moyen sur 49
		// cases
    /*
     * 1 1 2 2 2 1 1 1 2 3 4 3 2 1 2 3 4 5 4 3 2 2 4 5 8 5 4 2 2 3 4 5 4 3 2 1 2
     * 3 4 3 2 1 1 1 2 2 2 1 1
     */
		OutputCanvas.mMatriceConv49[0][0] = 1 / 128f;
		OutputCanvas.mMatriceConv49[0][1] = 1 / 128f;
		OutputCanvas.mMatriceConv49[0][2] = 2 / 128f;
		OutputCanvas.mMatriceConv49[0][3] = 2 / 128f;
		OutputCanvas.mMatriceConv49[0][4] = 2 / 128f;
		OutputCanvas.mMatriceConv49[0][5] = 1 / 128f;
		OutputCanvas.mMatriceConv49[0][6] = 1 / 128f;

		OutputCanvas.mMatriceConv49[1][0] = 1 / 128f;
		OutputCanvas.mMatriceConv49[1][1] = 2 / 128f;
		OutputCanvas.mMatriceConv49[1][2] = 3 / 128f;
		OutputCanvas.mMatriceConv49[1][3] = 4 / 128f;
		OutputCanvas.mMatriceConv49[1][4] = 3 / 128f;
		OutputCanvas.mMatriceConv49[1][5] = 2 / 128f;
		OutputCanvas.mMatriceConv49[1][6] = 1 / 128f;

		OutputCanvas.mMatriceConv49[2][0] = 2 / 128f;
		OutputCanvas.mMatriceConv49[2][1] = 3 / 128f;
		OutputCanvas.mMatriceConv49[2][2] = 4 / 128f;
		OutputCanvas.mMatriceConv49[2][3] = 5 / 128f;
		OutputCanvas.mMatriceConv49[2][4] = 4 / 128f;
		OutputCanvas.mMatriceConv49[2][5] = 3 / 128f;
		OutputCanvas.mMatriceConv49[2][6] = 2 / 128f;

		OutputCanvas.mMatriceConv49[3][0] = 2 / 128f;
		OutputCanvas.mMatriceConv49[3][1] = 4 / 128f;
		OutputCanvas.mMatriceConv49[3][2] = 5 / 128f;
		OutputCanvas.mMatriceConv49[3][3] = 8 / 128f;
		OutputCanvas.mMatriceConv49[3][4] = 5 / 128f;
		OutputCanvas.mMatriceConv49[3][5] = 4 / 128f;
		OutputCanvas.mMatriceConv49[3][6] = 2 / 128f;

		OutputCanvas.mMatriceConv49[4][0] = 2 / 128f;
		OutputCanvas.mMatriceConv49[4][1] = 3 / 128f;
		OutputCanvas.mMatriceConv49[4][2] = 4 / 128f;
		OutputCanvas.mMatriceConv49[4][3] = 5 / 128f;
		OutputCanvas.mMatriceConv49[4][4] = 4 / 128f;
		OutputCanvas.mMatriceConv49[4][5] = 3 / 128f;
		OutputCanvas.mMatriceConv49[4][6] = 2 / 128f;

		OutputCanvas.mMatriceConv49[5][0] = 1 / 128f;
		OutputCanvas.mMatriceConv49[5][1] = 2 / 128f;
		OutputCanvas.mMatriceConv49[5][2] = 3 / 128f;
		OutputCanvas.mMatriceConv49[5][3] = 4 / 128f;
		OutputCanvas.mMatriceConv49[5][4] = 3 / 128f;
		OutputCanvas.mMatriceConv49[5][5] = 2 / 128f;
		OutputCanvas.mMatriceConv49[5][6] = 1 / 128f;

		OutputCanvas.mMatriceConv49[6][0] = 1 / 128f;
		OutputCanvas.mMatriceConv49[6][1] = 1 / 128f;
		OutputCanvas.mMatriceConv49[6][2] = 2 / 128f;
		OutputCanvas.mMatriceConv49[6][3] = 2 / 128f;
		OutputCanvas.mMatriceConv49[6][4] = 2 / 128f;
		OutputCanvas.mMatriceConv49[6][5] = 1 / 128f;
		OutputCanvas.mMatriceConv49[6][6] = 1 / 128f;



		backBufferImage = new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
		// I used Graphics2D instead of Graphics here, because its more flexible and lets you do more things.
//		backBufferGraphics = (Graphics2D) backBufferImage.getGraphics();
//		WritableRaster outRaster = backBufferImage.getRaster().createCompatibleWritableRaster(mDimension.width, mDimension.height);
//		outPixels   =  outRaster.getDataBuffer();

		mSuspendu = false;
		repaint();
	}


	/******************************************************************************
	 * Titre : void paint(Graphics g) Description : Surcharge de la fonction qui
	 * est appelé lorsque le composant doit être redessiné
	 ******************************************************************************/
	@Override
	public void paint(Graphics pGraphics) {
		int i, j;
		for (i = 0; i < mDimension.width; i++) {
			for (j = 0; j < mDimension.height; j++) {
				if(updateOnGraphic[i][j]) {
					backBufferImage.setRGB(i, j, mCouleurs[i][j]);
				}
				updateOnGraphic[i][j] = false;
			}
		}
		pGraphics.drawImage(backBufferImage, 0,0, null);
	}

	/******************************************************************************
	 * Titre : void colorer_case(int x, int y, Color c) Description : Cette
	 * fonction va colorer le pixel correspondant et mettre a jour le tabmleau des
	 * couleurs
	 ******************************************************************************/
	public void setCouleur(int x, int y, int red, int green, int blue, int pTaille) {

		mCouleurs[x][y] = Utils.toRGB(red, green, blue);
		updateOnGraphic[x][y] = true;

		float[][] convMatrix = null;

		if(pTaille == 1)convMatrix = OutputCanvas.mMatriceConv9;
		if(pTaille == 2)convMatrix = OutputCanvas.mMatriceConv25;
		if(pTaille == 3)convMatrix = OutputCanvas.mMatriceConv49;

		// on fait diffuser la couleur (phéromones):
		if(pTaille > 0 && convMatrix != null) {
			//pour chaque pixel a lisser
			//couleur = ancienne val - mat*ancienneValDuPixelModifie+mat*newValDUPixel


			int virtualSize = 1 + pTaille * 2;
			int pTaille2 = pTaille * 2;
			int pTailleCoordinateIndex = pTaille - 1;

			for (int i = 0; i < virtualSize; i++) {
				for (int j = 0; j < virtualSize; j++) {
					float R, G, B;
					R = G = B = 0f;

					for (int k = 0; k < virtualSize; k++) {
						int m = bufferedCoordinatesMSmooth[pTailleCoordinateIndex][x][i][k];
						for (int l = 0; l < virtualSize; l++) {
							int n = bufferedCoordinatesNSmooth[pTailleCoordinateIndex][y][j][l];

							R += convMatrix[k][l] * Utils.getRed(mCouleurs[m][n]);
							G += convMatrix[k][l] * Utils.getGreen(mCouleurs[m][n]);
							B += convMatrix[k][l] * Utils.getBlue(mCouleurs[m][n]);
						}
					}


					int m = bufferedCoordinatesMSmoothDest[pTailleCoordinateIndex][x][i];
					int n = bufferedCoordinatesNSmoothDest[pTailleCoordinateIndex][y][j];
					mCouleurs[m][n] = Utils.toRGB((int) R, (int) G, (int) B);
					updateOnGraphic[m][n] = true;
				}
			}
		}

		repaint();
	}

	/******************************************************************************
	 * Titre : setSupendu Description : Cette fonction change l'état de suspension
	 ******************************************************************************/

	public void toggleSimulation() {
		mSuspendu = !mSuspendu;
		if (!mSuspendu) {
			repaint();
		}
	}
}
