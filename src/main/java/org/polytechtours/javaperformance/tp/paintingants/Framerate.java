package org.polytechtours.javaperformance.tp.paintingants;

import java.util.concurrent.atomic.AtomicLong;

public class Framerate {

	private AtomicLong mCompteur = new AtomicLong(0);
	private Object mMutexCompteur = new Object();
	private long last = 0;

	/****************************************************************************/
	/**
	 * incrémenter le compteur
	 */
	public void compteur(int sizeIncrement) {
		mCompteur.addAndGet(sizeIncrement);
	}

	public long reset(){
		last = mCompteur.get();
		mCompteur.set(0);
		return last;
	}

	public long getLast() {
		return last;
	}

	public long get() {
		return mCompteur.get();
	}
}
