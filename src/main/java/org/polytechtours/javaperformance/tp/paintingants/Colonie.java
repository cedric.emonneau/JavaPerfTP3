package org.polytechtours.javaperformance.tp.paintingants;

/*
 * Colonie.java
 *
 * Created on 11 avril 2007, 16:35
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

import java.awt.*;
import java.util.StringTokenizer;
import java.util.Vector;

public class Colonie implements Runnable {

	private Boolean mContinue = Boolean.TRUE;
	private Vector<Fourmi> mColonie;
	private Application mApplis;
	private Framerate framerate;
	private OutputCanvas canvas;

	/**
	 * Creates a new instance of Colonie
	 */
	public Colonie(Application pApplis, Framerate framerate, OutputCanvas canvas) {
		this.mColonie = new Vector<>();
		this.mApplis = pApplis;
		this.framerate = framerate;
		this.canvas = canvas;

		this.readParameterFourmis();
	}


	// =========================================================================
	// lecture des paramètres de l'applet
	private void readParameterFourmis() {
		String lChaine;
		int R, G, B;
		Color lCouleurDeposee, lCouleurSuivie;
		Fourmi lFourmi;
		float lProbaTD, lProbaG, lProbaD, lProbaSuivre, lSeuilLuminance;
		char lTypeDeplacement = ' ';
		int lInitDirection, lTaille;
		float lInit_x, lInit_y;
		int lNbFourmis = -1;

		// Lecture des paramètres des fourmis

		// Lecture du seuil de luminance
		// <PARAM NAME="SeuilLuminance" VALUE="N">
		// N : seuil de luminance : -1 = random(2..60), x..y = random(x..y)
		lChaine = mApplis.getParameter("SeuilLuminance");
		if (lChaine != null) {
			lSeuilLuminance = Parameters.readFloatParameter(lChaine);
		} else {
			// si seuil de luminance n'est pas défini
			lSeuilLuminance = 40f;
		}
		System.out.println("Seuil de luminance:" + lSeuilLuminance);

		// Lecture du nombre de fourmis :
		// <PARAM NAME="NbFourmis" VALUE="N">
		// N : nombre de fourmis : -1 = random(2..6), x..y = random(x..y)
		lChaine = mApplis.getParameter("NbFourmis");
		if (lChaine != null) {
			lNbFourmis = Parameters.readIntParameter(lChaine);
		} else {
			// si le parametre NbFourmis n'est pas défini
			lNbFourmis = -1;
		}
		// si le parametre NbFourmis n'est pas défini ou alors s'il vaut -1 :
		if (lNbFourmis == -1) {
			// Le nombre de fourmis est aléatoire entre 2 et 6 !
			lNbFourmis = (int) (Math.random() * 5) + 2;
		}

		// <PARAM NAME="Fourmis"
		// VALUE="(255,0,0)(255,255,255)(20,40,1)([d|o],0.2,0.6,0.2,0.8)">
		// (R,G,B) de la couleur déposée : -1 = random(0...255); x:y = random(x...y)
		// (R,G,B) de la couleur suivie : -1 = random(0...255); x:y = random(x...y)
		// (x,y,d,t) position , direction initiale et taille du trait
		// x,y = 0.0 ... 1.0 : -1 = random(0.0 ... 1.0); x:y = random(x...y)
		// d = 7 0 1
		// 6 X 2
		// 5 4 3 : -1 = random(0...7); x:y = random(x...y)
		// t = 0, 1, 2, 3 : -1 = random(0...3); x:y = random(x...y)
		//
		// (type deplacement,proba gauche,proba tout droit,proba droite,proba
		// suivre)
		// type deplacement = o/d : -1 = random(o/d)
		// probas : -1 = random(0.0 ... 1.0); x:y = random(x...y)

		lChaine = mApplis.getParameter("Fourmis");
		if (lChaine != null) {
			// on affiche la chaine de parametres
			System.out.println("Paramètres:" + lChaine);

			// on va compter le nombre de fourmis dans la chaine de parametres :
			lNbFourmis = 0;
			// chaine de paramètres pour une fourmi
			StringTokenizer lSTFourmi = new StringTokenizer(lChaine, ";");
			while (lSTFourmi.hasMoreTokens()) {
				// chaine de parametres de couleur et proba
				StringTokenizer lSTParam = new StringTokenizer(lSTFourmi.nextToken(), "()");
				// lecture de la couleur déposée
				StringTokenizer lSTCouleurDéposée = new StringTokenizer(lSTParam.nextToken(), ",");
				R = Parameters.readIntParameter(lSTCouleurDéposée.nextToken());
				if (R == -1) {
					R = (int) (Math.random() * 256);
				}

				G = Parameters.readIntParameter(lSTCouleurDéposée.nextToken());
				if (G == -1) {
					G = (int) (Math.random() * 256);
				}
				B = Parameters.readIntParameter(lSTCouleurDéposée.nextToken());
				if (B == -1) {
					B = (int) (Math.random() * 256);
				}
				lCouleurDeposee = new Color(R, G, B);
				System.out.print("Parametres de la fourmi " + lNbFourmis + ":(" + R + "," + G + "," + B + ")");

				// lecture de la couleur suivie
				StringTokenizer lSTCouleurSuivi = new StringTokenizer(lSTParam.nextToken(), ",");
				R = Parameters.readIntParameter(lSTCouleurSuivi.nextToken());
				G = Parameters.readIntParameter(lSTCouleurSuivi.nextToken());
				B = Parameters.readIntParameter(lSTCouleurSuivi.nextToken());
				lCouleurSuivie = new Color(R, G, B);
				System.out.print("(" + R + "," + G + "," + B + ")");

				// lecture de la position de la direction de départ et de la taille de
				// la trace
				StringTokenizer lSTDéplacement = new StringTokenizer(lSTParam.nextToken(), ",");
				lInit_x = Parameters.readFloatParameter(lSTDéplacement.nextToken());
				if (lInit_x < 0.0 || lInit_x > 1.0) {
					lInit_x = (float) Math.random();
				}
				lInit_y = Parameters.readFloatParameter(lSTDéplacement.nextToken());
				if (lInit_y < 0.0 || lInit_y > 1.0) {
					lInit_y = (float) Math.random();
				}
				lInitDirection = Parameters.readIntParameter(lSTDéplacement.nextToken());
				if (lInitDirection < 0 || lInitDirection > 7) {
					lInitDirection = (int) (Math.random() * 8);
				}
				lTaille = Parameters.readIntParameter(lSTDéplacement.nextToken());
				if (lTaille < 0 || lTaille > 3) {
					lTaille = (int) (Math.random() * 4);
				}
				System.out.print("(" + lInit_x + "," + lInit_y + "," + lInitDirection + "," + lTaille + ")");

				// lecture des probas
				StringTokenizer lSTProbas = new StringTokenizer(lSTParam.nextToken(), ",");
				lTypeDeplacement = lSTProbas.nextToken().charAt(0);

				if (lTypeDeplacement != 'o' && lTypeDeplacement != 'd') {
					if (Math.random() < 0.5) {
						lTypeDeplacement = 'o';
					} else {
						lTypeDeplacement = 'd';
					}
				}

				lProbaG = Parameters.readFloatParameter(lSTProbas.nextToken());
				lProbaTD = Parameters.readFloatParameter(lSTProbas.nextToken());
				lProbaD = Parameters.readFloatParameter(lSTProbas.nextToken());
				lProbaSuivre = Parameters.readFloatParameter(lSTProbas.nextToken());
				// on normalise au cas ou
				float lSomme = lProbaG + lProbaTD + lProbaD;
				lProbaG /= lSomme;
				lProbaTD /= lSomme;
				lProbaD /= lSomme;

				System.out.println(
						"(" + lTypeDeplacement + "," + lProbaG + "," + lProbaTD + "," + lProbaD + "," + lProbaSuivre + ");");

				// création de la fourmi
				lFourmi = new Fourmi(lCouleurDeposee, lCouleurSuivie, lProbaTD, lProbaG, lProbaD, lProbaSuivre, this.canvas,
						lTypeDeplacement, lInit_x, lInit_y, lInitDirection, lTaille, lSeuilLuminance, this.mApplis);
				mColonie.addElement(lFourmi);
				lNbFourmis++;
			}
		} else // initialisation aléatoire des fourmis
		{

			int i;
			Color lTabColor[] = new Color[lNbFourmis];
			int lColor;

			// initialisation aléatoire de la couleur de chaque fourmi
			for (i = 0; i < lNbFourmis; i++) {
				R = (int) (Math.random() * 256);
				G = (int) (Math.random() * 256);
				B = (int) (Math.random() * 256);
				lTabColor[i] = new Color(R, G, B);
			}

			// construction des fourmis
			for (i = 0; i < lNbFourmis; i++) {
				// la couleur suivie est la couleur d'une autre fourmi
				lColor = (int) (Math.random() * lNbFourmis);
				if (i == lColor) {
					lColor = (lColor + 1) % lNbFourmis;
				}

				// une chance sur deux d'avoir un déplacement perpendiculaire
				if ((float) Math.random() < 0.5f) {
					lTypeDeplacement = 'd';
				} else {
					lTypeDeplacement = 'o';
				}

				// position initiale
				lInit_x = (float) (Math.random()); // *mOutputCanvas.getLargeur()
				lInit_y = (float) (Math.random()); // *mOutputCanvas.getHauteur()

				// direction initiale
				lInitDirection = (int) (Math.random() * 8);

				// taille du trait
				lTaille = (int) (Math.random() * 4);

				// proba de déplacement :
				lProbaTD = (float) (Math.random());
				lProbaG = (float) (Math.random() * (1.0 - lProbaTD));
				lProbaD = (float) (1.0 - (lProbaTD + lProbaG));
				lProbaSuivre = (float) (0.5 + 0.5 * Math.random());

				System.out.print(
						"Random:(" + lTabColor[i].getRed() + "," + lTabColor[i].getGreen() + "," + lTabColor[i].getBlue() + ")");
				System.out.print("(" + lTabColor[lColor].getRed() + "," + lTabColor[lColor].getGreen() + ","
						+ lTabColor[lColor].getBlue() + ")");
				System.out.print("(" + lInit_x + "," + lInit_y + "," + lInitDirection + "," + lTaille + ")");
				System.out.println(
						"(" + lTypeDeplacement + "," + lProbaG + "," + lProbaTD + "," + lProbaD + "," + lProbaSuivre + ");");

				// création et ajout de la fourmi dans la colonie
				lFourmi = new Fourmi(lTabColor[i], lTabColor[lColor], lProbaTD, lProbaG, lProbaD, lProbaSuivre, this.canvas,
						lTypeDeplacement, lInit_x, lInit_y, lInitDirection, lTaille, lSeuilLuminance, this.mApplis);
				mColonie.addElement(lFourmi);
			}
		}

	}


	public void pleaseStop() {
		mContinue = false;
	}

	@Override
	public void run() {
		int frame = 0;
		while (mContinue) {
			if (!mApplis.getPause()) {
				for (int i = 0; i < mColonie.size(); ++i) {
					mColonie.get(i).deplacer();
				}
				framerate.compteur(mColonie.size());

				//TODO pas en dur....
//				if(framerate.getLast() != 0 && frame%(framerate.getLast()/120)==0)
//					canvas.repaint();
			}
			++frame;
		}
	}

}
